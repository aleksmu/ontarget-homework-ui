import React, {useEffect, useState} from "react";
import axios from "axios";


function useEndpoint(req) {
    const [res, setRes] = useState({
        data: null,
        complete: false,
        pending: false,
        error: false
    });
    useEffect(
        () => {
            setRes({
                data: null,
                pending: true,
                error: false,
                complete: false
            });
            axios(req)
                .then(res =>
                    setRes({
                        data: res.data,
                        pending: false,
                        error: false,
                        complete: true
                    })
                )
                .catch(() =>
                    setRes({
                        data: null,
                        pending: false,
                        error: true,
                        complete: true
                    })
                );
        },
        [req.url]
    );
    return res;
}

function ChairList(chairsApi) {
    const chairs = useEndpoint({
        method: "GET",
        url: `${chairsApi.url}`
    });
    console.log(chairsApi)
    return (
        <div className='chairList'>
            <div>
                {(chairs.pending && "Loading...") || (chairs.complete &&
                    <ol>
                        {chairs.data.map(chair =>
                            <li key={chair.id}>{chair.name}</li>
                        )}
                    </ol>
                )}
            </div>
        </div>
    );
}
export default ChairList