import React, {useState} from 'react';
import './App.css';
import ChairList from "./components/ChairList";

function App() {
  const [showArmchair, setArmchair] = useState(false);
  const [showTabouret, setTabouret] = useState(false);

  return (
      <div>
        <button className='myButton' onClick={() => setArmchair(!showArmchair)}>
          {!showArmchair ? 'Show all armchairs' : 'Hide all armchairs'}
        </button>
        {showArmchair && <ChairList url={'/api/armchair'}/>}
        <button className='myButton' onClick={() => setTabouret(!showTabouret)}>
          {!showTabouret ? 'Show all tabourets' : 'Hide all tabourets'}
        </button>
        {showTabouret && <ChairList url={'/api/tabouret'}/>}
      </div>
  );
}

export default App;
