![Preview of UI](pictures/preview.gif)

## What is this
Frontend part of the homework


## Available Scripts

In the project directory, you need to run following scripts:

### `npm install`

and then

### `npm start`

It runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.